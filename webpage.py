from flask import Flask, render_template, request
import requests
from flask_apscheduler import APScheduler


response = None
currencies = None

app = Flask(__name__)
scheduler = APScheduler()

API_URL = "https://openexchangerates.org/api/latest.json"
API_KEY = "8844ef9338df4629a398ed53787d08b9"
response = requests.get(f"{API_URL}?app_id={API_KEY}")
data = response.json()


@scheduler.task("interval", id="get_response", hours=1)
def get_response():
    global response
    response = requests.get(f"{API_URL}?app_id={API_KEY}")


@app.route("/", methods=["GET"])
def index():
    country = request.args.get("country")
    global currencies

    if response.status_code != 200:
        return "Unable to retrieve exchange rates."
    data = response.json()
    rates = data["rates"]
    currencies = {
        "AED": None,
        "BHD": None,
        "BTC": None,
        "EGP": None,
        "EUR": None,
        "KWD": None,
        "USD": None,
    }
    for currency in currencies:
        for rate in rates:
            if currency == rate:
                currencies[currency] = rates[rate]
    countries = {
        "United_Arab_Emirates": "AED",
        "Bahrain": "BHD",
        "Bitcoin": "BTC",
        "Egypt": "EGP",
        "European_Union": "EUR",
        "Kuwait": "KWD",
        "United_States": "USD",
    }
    if not country:
        country = "United_States"
    return render_template(
        "index.html", currencies=currencies, countries=countries, country=country
    )


@app.route("/convert", methods=["GET", "POST"])
def convert():
    if request.method == "POST":
        from_currency = request.form.get("from_currency")
        to_currency = request.form.get("to_currency")
        converted_amount = request.form.get("converted_amount")
        try:
            amount = round(
                float(converted_amount)
                * currencies[to_currency]
                / currencies[from_currency],
                2,
            )
            return render_template(
                "result.html",
                from_currency=from_currency,
                to_currency=to_currency,
                amount=amount,
                converted_amount=converted_amount,
            )
        except:
            return render_template("convert.html", currencies=currencies)
    else:
        return render_template("convert.html", currencies=currencies)


if __name__ == "__main__":
    scheduler.init_app(app)
    scheduler.start()
    get_response()
    app.run(debug=True)
