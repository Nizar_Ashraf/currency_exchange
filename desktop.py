import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
import requests
import schedule
import threading


class MyGUI:
    def __init__(self):
        self.data = None
        self.currencies = {
            "AED": None,
            "BHD": None,
            "BTC": None,
            "EGP": None,
            "EUR": None,
            "KWD": None,
            "USD": None,
            "SAR": None,
        }
        self.countries = {
            "United_Arab_Emirates": "AED",
            "Bahrain": "BHD",
            "Bitcoin": "BTC",
            "Egypt": "EGP",
            "European_Union": "EUR",
            "Kuwait": "KWD",
            "United_States": "USD",
            "Saudi": "SAR",
        }

        self.root = tk.Tk()
        self.root.title("Currency Exchange Rates")

        self.label = tk.Label(
            self.root, text="Currency Exchange Rates", font=("Arial", 20, "bold")
        )
        self.label.pack(padx=10, pady=10)

        self.button = tk.Button(
            self.root,
            text="Refresh",
            font=("Arial", 16),
            command=self.refresh_rates,
        )
        self.button.pack(side="top", padx=10, pady=10, anchor="center")

        self.selected_country = tk.StringVar(self.root)
        self.selected_country.set("United_States")
        self.menu = tk.OptionMenu(
            self.root,
            self.selected_country,
            *self.countries.keys(),
            command=self.update_base_currency,
        )
        self.menu.pack(padx=10, pady=10)

        self.table_of_currencies = ttk.Treeview(
            self.root, columns=("Currency", "Exchange Rate")
        )
        self.table_of_currencies.heading("#0", text="Country")
        self.table_of_currencies.heading("#1", text="Currency")
        self.table_of_currencies.heading("#2", text="Exchange Rate")
        self.table_of_currencies.pack(padx=10, pady=10)

        self.from_Currency = tk.Label(
            self.root, text="From Currency", font=("Arial", 14)
        )
        self.from_Currency.pack(padx=10, pady=10, side="left", anchor="nw")

        self.convert_from = tk.StringVar(self.root)
        self.convert_from.set("USD")
        self.from_Currency_menu = tk.OptionMenu(
            self.root, self.convert_from, *self.currencies.keys()
        )
        self.from_Currency_menu.pack(padx=10, pady=10, side="left", anchor="n")

        self.to_Currency = tk.Label(self.root, text="To Currency", font=("Arial", 14))
        self.to_Currency.pack(padx=10, pady=10, side="left", anchor="nw")

        self.convert_to = tk.StringVar(self.root)
        self.convert_to.set("EGP")
        self.to_Currency_menu = tk.OptionMenu(
            self.root, self.convert_to, *self.currencies.keys()
        )
        self.to_Currency_menu.pack(padx=10, pady=10, side="left", anchor="n")

        self.button = tk.Button(
            self.root,
            text="Convert Currency",
            font=("Arial", 16),
            command=self.convert_currency,
        )
        self.button.pack(padx=10, pady=10, side="left", anchor="n")

        self.converted_amount = tk.Text(self.root, height=1, width=10)
        self.converted_amount.pack(padx=10, pady=10, side="left", anchor="w")

        self.result = tk.Label(
            self.root, text=" 00.00 ABC = ABC 00.00", font=("Arial", 14)
        )
        self.result.pack(padx=10, pady=10)

        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)

        self.root.mainloop()

    def on_closing(self):
        if messagebox.askyesno(
            title="Quit?", message="Do you really wants to close this?"
        ):
            self.root.destroy()

    def refresh_rates_thread(self):
        try:
            API_URL = "https://openexchangerates.org/api/latest.json"
            API_KEY = "8844ef9338df4629a398ed53787d08b9"
            response = requests.get(f"{API_URL}?app_id={API_KEY}")
            if response.status_code != 200:
                messagebox.showerror("Error", "Unable to retrieve exchange rates.")
                return
            self.data = response.json()
            rates = self.data["rates"]
            for currency in self.currencies:
                for rate in rates:
                    if currency == rate:
                        self.currencies[currency] = float(rates[rate])
            self.put_data_in_table()
        except:
            messagebox.showerror(
                title="Error", message="Error While connecting to the internet"
            )

    def put_data_in_table(self):
        try:
            for item in self.table_of_currencies.get_children():
                self.table_of_currencies.delete(item)
            for country in self.countries:
                self.table_of_currencies.insert(
                    "",
                    "end",
                    text=country,
                    values=(
                        self.countries[country],
                        self.currencies[self.countries[country]],
                    ),
                )
            self.table_of_currencies.column("Currency", anchor="center")
            self.table_of_currencies.column("Exchange Rate", anchor="center")
        except:
            messagebox.showerror(
                title="Error", message="Error while putting Data in table"
            )

    def update_base_currency(self, event=None):
        try:
            rates = self.data["rates"]
            for currency in self.currencies:
                for rate in rates:
                    if currency == rate:
                        self.currencies[currency] = float(rates[rate])
            self.selected_country_value = self.selected_country.get()
            for currency_key, currency_value in self.currencies.items():
                if self.countries[self.selected_country_value] != currency_key:
                    currency_value = round(
                        currency_value
                        / self.currencies[self.countries[self.selected_country_value]],
                        5,
                    )
                    self.currencies[currency_key] = currency_value
            self.currencies[self.countries[self.selected_country_value]] = 1
            self.put_data_in_table()
        except:
            messagebox.showerror(
                title="Error",
                message="Error while updating Base Currency Make sure you are connected to the internet",
            )

    def convert_currency(self):
        try:
            rates = self.data["rates"]
            for currency in self.currencies:
                for rate in rates:
                    if currency == rate:
                        self.currencies[currency] = float(rates[rate])
            converted_amount_value = float(self.converted_amount.get("1.0", tk.END))
            self.amount = round(
                converted_amount_value
                * self.currencies[self.convert_to.get()]
                / self.currencies[self.convert_from.get()],
                5,
            )
            self.result.destroy()
            self.result = tk.Label(
                self.root,
                text=f" {converted_amount_value} {self.convert_from.get()} = {self.amount} {self.convert_to.get()}",
                font=("Arial", 14),
                anchor="s",
            )

            self.result.pack(padx=10, pady=10, anchor="center")
        except TypeError:
            messagebox.showerror(
                title="Error", message="Make sure you are connected to the internet"
            )
        except ValueError:
            messagebox.showerror(
                title="Error", message="Please Enter a valid input (POSITIVE NUMBER)"
            )

    def refresh_rates(self):
        t = threading.Thread(target=self.refresh_rates_thread)
        t.start()
        self.selected_country.set("United_States")

    def run_scheduler(self):
        schedule.every().hour.do(self.refresh_rates)


MyGUI()
